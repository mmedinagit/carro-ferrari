﻿using System;

namespace Carro
{
    class Program
    {
        static void Main(string[] args)
        {
            
            decimal  Total = 0, Total2 = 0, Total3 = 0, Total4 = 0, Total5 = 0, Total6 = 0;
            carro Caractcarro;
            Caractcarro = new carro();
            Console.WriteLine("Colores Disponibles: Rojo, Azul, Negro, Blanco ");
            Console.Write("Introduzca el color deseado: ");
            string color = Console.ReadLine();
            Caractcarro.Color=color;
            decimal precio = Caractcarro._Precio(50000m);
            string marca = Caractcarro._Marca("Ferrari");
            string submarca = Caractcarro._SubMarca("250 GT Berlinetta");
            
            
                    if (Caractcarro.Color == "Rojo")
                        {
                            Total = precio +=15000m;
                        } 
                    else if (Caractcarro.Color == "Azul")
                        {
                            Total = precio +=16000m;
                        }
                    else if (Caractcarro.Color == "Negro")
                        {
                            Total = precio +=17000m;
                        }
                     else if (Caractcarro.Color == "Blanco")
                        {
                            Total = precio +=18000m;
                        }     
                    else
                        {
                            Console.WriteLine("\nEl Color indicado no esta en el catalogo, Favor de verificar");
                        }
/*------------------------------------------------------------------------------------------- */
            Console.WriteLine("\nJuego de Rines Disponibles: 1.- 16 Pulg., 2.- 18 Pulg., 3.- 20 Pulg.");
            Console.Write("Introduzca el numero de acuerdo a las Puldas de su juego de rines: ");
            int Rines = int.Parse(Console.ReadLine());
            Caractcarro.TipoRin = Rines;

                         if (Caractcarro.TipoRin == 1)
                        {
                            Total2 = Total += 15000m;
                        } 
                    else if (Caractcarro.TipoRin == 2)
                        {
                            Total2 = Total +=16000m;
                        }
                    else if (Caractcarro.TipoRin == 3)
                        {
                            Total2 = Total +=17000m;
                        }
                     else if (Caractcarro.TipoRin == 4)
                        {
                            Total2 = Total +=18000m;
                        }     
                    else
                        {
                            Console.WriteLine("\nEl tipo de rines indicado no esta en el catalogo, Favor de verificar");
                        }

                        
                     
/*------------------------------------------------------------------------------------------- */
            Console.WriteLine("\nAccesorio de alerón: Si (para Aceptar),  No (para rechazar)?");
            string Ale = Console.ReadLine();
            Caractcarro.Alerón = Ale;

                         if (Caractcarro.Alerón == "Si")
                        {
                            Total3 = Total2 += 15000m;
                         } 
                    else if (Caractcarro.Alerón == "No")
                        {
                            Total3 = Total2 +=0000m;
                        }
                    else
                        {
                            Console.WriteLine("\nFavor de verificar su respuesta");
                        }

/*------------------------------------------------------------------------------------------- */
            Console.WriteLine("\nIndicar tipo de transmisión: 1.- Automatica,  2.- Manual");
            Console.Write("Introduzca el numero de acuerdo al tipo de transmisión: ");
            int Tran = int.Parse(Console.ReadLine());
            Caractcarro.Transmisión = Tran;

                         if (Caractcarro.Transmisión == 1)
                        {
                            Total4 = Total3 += 16000m;
                        } 
                    else if (Caractcarro.Transmisión == 2)
                        {
                            Total4 = Total3 +=15000m;
                        }
                    else
                        {
                            Console.WriteLine("\nFavor de verificar su respuesta");
                        }   

/*------------------------------------------------------------------------------------------- */
             Console.WriteLine("\nSe requiere aire acondicionado?: Si (para Aceptar),  No (para rechazar)?");
            string AA = Console.ReadLine();
            Caractcarro.AireAcondicionado = AA;

                         if (Caractcarro.AireAcondicionado == "Si")
                        {
                            Total5 = Total4 += 15000m;
                        } 
                    else if (Caractcarro.AireAcondicionado == "No")
                        {
                            Total5 = Total4 +=0000m;
                        }
                    else
                        {
                            Console.WriteLine("\nFavor de verificar su respuesta");
                        }    

/*------------------------------------------------------------------------------------------- */
             Console.WriteLine("\nSe requieren Elevadores Electricos?: Si (para Aceptar),  No (para rechazar)?");
            string EE = Console.ReadLine();
            Caractcarro.ElevadoresElectricos = EE;

                         if (Caractcarro.ElevadoresElectricos == "Si")
                        {
                            Total6 = Total5 += 15000m;
                        } 
                    else if (Caractcarro.ElevadoresElectricos == "No")
                        {
                            Total6 = Total5 +=0000m;
                        }
                    else
                        {
                            Console.WriteLine("\nFavor de verificar su respuesta");
                        }    
                Console.WriteLine("\n********************************************");
                Console.WriteLine("Marca del Carro:" + marca);
                Console.WriteLine("\nSubMarca del Carro:" + submarca);
                Console.WriteLine("\nPrecio base del Carro:" + precio);
                Console.WriteLine("\nLas especificaciones del carro son las siguientes:" +  "\nColor: " + Caractcarro.Color + "\nTipo de Rines (opción): " + Caractcarro.TipoRin + "\nCon alerón: " + Caractcarro.Alerón + "\nAire Acondicionado: " + Caractcarro.AireAcondicionado + "\nTipo de Transmisión (opción):" + Caractcarro.Transmisión +  "\nElevadores Electricos: " + Caractcarro.ElevadoresElectricos);
                Console.WriteLine("\nPrecio Total es: " + Total6.ToString("C"));
                Console.ReadKey(); 


        }
    }

    /*-------------------------------CLASE------------------------------------------ */
    public class carro /*se declara el nombre de la Clase */
    {
        /*-----------------------------CONSTRUCTOR--------------------------------- */
        public carro()
        {
            
            Color="";
            TipoRin= 0;
            Alerón="";
            Transmisión= 0;
            AireAcondicionado="";
            ElevadoresElectricos="";
            PrecioBase=50000m;

        }
        
        public string Color;/*Propiedades de la variable (atributo)*/
        public int TipoRin;
        public string Alerón;
        public int Transmisión;
        public decimal PrecioBase;
        public string AireAcondicionado;
        public string ElevadoresElectricos;
       
        /*----------------------------METODO---------------------------------------- */
        public string _Marca(string Marca)
        {
            return Marca;
            
        }
        public string _SubMarca(string SubMarca)
        {
           return SubMarca;
        }
        
        public decimal _Precio(decimal PrecioNeto)
        {        
           return PrecioNeto;
        }
    }
}
